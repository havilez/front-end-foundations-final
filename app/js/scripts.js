// $(function(){
//   // retrieve all elements  within element with class 'fadein' .
//   // then hide all img tags except the first one
//   // so only first element displays?
//     $('.fadein img:gt(0)').hide();
//   // fade in the first
//     setInterval(function(){
//       // fade out first child node within element with class '.fadein'
//       // retrieve next img node witin element with class '.fadein' and display it
//       // append img node to end of list of img nodes within element with class '.fadein'
//       $('.fadein :first-child').fadeOut()
//          .next('img').fadeIn()
//          .end().appendTo('.fadein');}, 
//       3000);
// });

// retrieve all img elements within element with class '.fadein'
const imageNodes = document.querySelectorAll('.fadein img');

// convert node list to an array
const imagesArray = [...imageNodes];

// hide all img nodes within images array except current
var current = 0;

 hideImages(imagesArray,current);

 // delay callback to fade out current image and display the next image
 //IFF PASS GLOBAL variable  as parameter, you CANNOT change its value
 // within the function ,
// setInterval(switchPic  ,3000,imagesArray,current);
setInterval(switchPic  ,3000,imagesArray);


 /******* Function definitions ********/

function hideImages ( imgArray, tempIndex) {
  var currIndex = tempIndex;
   imgArray.forEach(function(imageItem, index,array) {
      if ( index  != currIndex) {
         imageItem.classList.add('fade-out');
        //  imageItem.style.display= 'none';
      }
        
      
  }
)}




function switchPic(imagesArray) {
   // FYI -- current is a global variable used within this function
   let imageItem = null;

   if (current < imagesArray.length) { 
  // fade out current image

   imageItem = imagesArray[current];


 
  if ( imageItem.classList.contains('fade-in') )
    imageItem.classList.remove('fade-in');

     imageItem.classList.add('fade-out');
  }

  // fade in next image in array
  if (current < imagesArray.length-1)
    current++;
  else
    current = 0;

  imageItem = imagesArray[current];

  if ( imageItem.classList.contains('fade-out') )
    imageItem.classList.remove('fade-out');
  imagesArray[current].classList.add('fade-in');
}







